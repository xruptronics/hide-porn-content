import os
import re

extension = ['mp4','mkv','avi','mov','3gp','webm','flv']

root_dir = '/'

def get_keywords():
	"""returns list of keywords"""
	keywords = []

	with open('keywords.txt') as kw_file_handle:
		keyword_lines = kw_file_handle.readlines()

	for keyword_line in keyword_lines:
		for keyword in keyword_line.split(','):
			keywords.append(keyword)

	return keywords


def get_name_of_videos():
	"""return list of video file names"""
	listed_videos = []
	for root,_,files in os.walk(root_dir):
		for file in files:
			if file[-3:] in extension:
				listed_videos.append((root,file))
			else:
				pass
	return listed_videos


def hide_unhide(command,keywords,listed_videos):
	"""perform hiding or unhiding operation"""
	if command == "hide":
		for root,hide_item in listed_videos:
			if re.search('^\.',hide_item) is None:
				for kw in keywords:
					if kw in hide_item.lower():
						try:
							os.chdir(root)
							os.rename(hide_item,'.'+hide_item)
							# hidden_files.append(root,'.'+hide_item)
							os.chdir(root_dir)
						except:
							pass
					else:
						pass
			else:
				pass
	elif command == "unhide":
		for root,hide_item in listed_videos:
			for kw in keywords:
				if kw in hide_item.lower():
					try:
						os.chdir(root)
						os.rename(hide_item,hide_item[1:])
						os.chdir(root_dir)
					except:
						pass	
				else:
					pass
	else:
		print "invalid command"
			
			


def main():
	command = raw_input("Enter 'hide' to hide or 'unhide' to unhide videos : ") 
	keywords = get_keywords()
	listed_videos = get_name_of_videos()
	hide_unhide(command,keywords,listed_videos)


if __name__ == "__main__":
	main()



